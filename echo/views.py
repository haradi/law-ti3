from django.shortcuts import render
from django.http import HttpResponse
# Create your views here.

def echo(request):
    param = request.GET.get('param', '')
    return HttpResponse(param)